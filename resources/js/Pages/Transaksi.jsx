import './css/transaksi.css'
import Navbar from '@/Components/navbar';
import gambar from './assets/gambar.jpg'
import logo from './assets/th.jpg'
const Transaksi = () => {
   
        return (
        <>
        <Navbar/>
         <div className="container">
      <div className="card-container">
        <div className="card">
          <img src={gambar} alt="Card 1" className="card-image" />
          <div className="card-text">Card 1</div>
        </div>
        <div className="card">
        <img src={gambar} alt="Card 1" className="card-image" />
          <div className="card-text">Card 2</div>
        </div>
        <div className="card">
        <img src={gambar} alt="Card 1" className="card-image" />
          <div className="card-text">Tempe Goreng</div>
          <div className="card-text-2">RP. 5.000</div>
        </div>
        <div className="card">
        <img src={gambar} alt="Card 1" className="card-image" />
          <div className="card-text">Card 4</div>
        </div>
        <div className="card">
        <img src={gambar} alt="Card 1" className="card-image" />
          <div className="card-text">Card 5</div>
        </div>
        <div className="card">
        <img src={gambar} alt="Card 1" className="card-image" />
          <div className="card-text">Card 6</div>
        </div>
        <div className="card">
        <img src={gambar} alt="Card 1" className="card-image" />
          <div className="card-text">Card 7</div>
        </div>
        <div className="card">
        <img src={gambar} alt="Card 1" className="card-image" />
          <div className="card-text">Card 8</div>
        </div>
        <div className="card">
        <img src={gambar} alt="Card 1" className="card-image" />
          <div className="card-text">Card 9</div>
        </div>
      </div>
      <div className="large-card">
     <div className="large-card-content">
    <img src={logo} alt="" className="logo" />
      <p className='pesanan'>Pesanan</p>
    <div className="list-item">
      <img src={gambar} alt="" className="foto" />
      <p className="nama">Ayam Goreng</p>
      <p className="x1">x1</p>
      <p className="harga">Harga 1</p>
    </div>
    <div className="list-item">
      <img src={gambar} alt="" className="foto" />
      <p className="nama">Nama Prxxxoduk 2</p>
      <p className="x1">x2</p>
      <p className="harga">Harga 2</p>
    </div>
    <div className="list-item">
      <img src={gambar} alt="" className="foto" />
      <p className="nama">Nama Prxxxoduk 2</p>
      <p className="x1">x2</p>
      <p className="harga">Harga 2</p>
    </div>
    <div className="list-item">
      <img src={gambar} alt="" className="foto" />
      <p className="nama">Nama Prxxxoduk 2</p>
      <p className="x1">x2</p>
      <p className="harga">Harga 2</p>
    </div>
    <button className="button">Clear Chart</button>
    <div className='save'>
       <button className='savebill'>Save Bill</button>
       <button className='printbill'>Print Bill</button>
    </div>
    <button className='charge'>Charge</button>
  </div>
</div></div>

       </> 
       )
}

// const NoNews = () => {
//     return (
//         <div>BELUM ADA BERITA HARI INI!!</div>
//     )
// }

// const NewsLists = ({  }) => {
//     return !news ? NoNews : isNews(news)
// }

export default Transaksi