// import { Link } from '@inertiajs/inertia-react'
import { Link } from '@inertiajs/inertia-react';
import './css/style.css'

const Navbar = () => {
  return (
    <>
    <div className="navbar bg-blue-500">
      <div className="flex-1">
      <Link  href="/food"  className="alan primary-content">Alan Resto</Link>

      </div>
    </div>
    <div className="navbar-2">
         <Link href="/food" className='alan-1'>Food</Link>
        <Link href="/transaksi" className='alan'>Transaksi</Link>
    </div>
    </>
  )
}


export default Navbar